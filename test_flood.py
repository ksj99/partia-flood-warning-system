# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 00:05:30 2018

@author: kiran
"""
from floodsystem.flood import *
from floodsystem.stationdata import MonitoringStation
 
def test_stations_level_over_threshold():
    """Function that tests the stations_level_over_threshold function by creating 5 MonitoringStation
    objects and checking if the values given by the function are correct"""

    #Create 5 MonitoringStation with known typical ranges
    station0 = MonitoringStation('station_id', 'measure_id', 'station0', 'coord', (1,2), 'riverA', 'town')
    station1 = MonitoringStation('station_id', 'measure_id', 'station1', 'coord', (1,2), 'riverB', 'town')
    station2 = MonitoringStation('station_id', 'measure_id', 'station2', 'coord', (1,2), 'riverC', 'town')
    station3 = MonitoringStation('station_id', 'measure_id', 'station3', 'coord', (1,2), 'riverD', 'town')
    station4 = MonitoringStation('station_id', 'measure_id', 'station4', 'coord', (1,2), 'riverE', 'town')
    
    #Attach known level data to stations
    station0.latest_level = 1
    station1.latest_level = 1.5
    station2.latest_level = 2
    station3.latest_level = 3
    station4.latest_level = None
    
    #Create a list of the MonitoringStation objects
    stations = [station0, station1, station2, station3, station4]
    
    #Use the function to create a list
    test_list = stations_level_over_threshold(stations,0.4)

    #Create the list that we know contains the correct values
    list_check = [('station1', 0.5), ('station2', 1.0), ('station3', 2.0)]

    #Assert the correctness of the results
    assert test_list == list_check
  
def test_stations_highest_rel_level():

    #Create 5 MonitoringStation with known typical ranges
    station0 = MonitoringStation('station_id', 'measure_id', 'station0', 'coord', (1,2), 'riverA', 'town')
    station1 = MonitoringStation('station_id', 'measure_id', 'station1', 'coord', (1,2), 'riverB', 'town')
    station2 = MonitoringStation('station_id', 'measure_id', 'station2', 'coord', (1,2), 'riverC', 'town')
    station3 = MonitoringStation('station_id', 'measure_id', 'station3', 'coord', (1,2), 'riverD', 'town')
    station4 = MonitoringStation('station_id', 'measure_id', 'station4', 'coord', (1,2), 'riverE', 'town')
    
    #Attach known level data to stations
    station0.latest_level = 1
    station1.latest_level = 1.5
    station2.latest_level = 2
    station3.latest_level = 3
    station4.latest_level = None
    
    #Create a list of the MonitoringStation objects
    stations = [station0, station1, station2, station3, station4]
    
    #Use the function to create a list
    test_list = stations_highest_rel_level(stations,3)

    #Create the list that we know contains the correct values
    list_check = [('station3',2),('station2',1),('station1',0.5)]

    #Assert the correctness of the results
    assert test_list == list_check