Our extension makes the system more usable by allowing the user to select the danger levels that are
of interest, rather than seeing everything at once, and having too much data to take in.  The user
can select the categories that they want to see by means of a list of check boxes which pop up when
the program is first run.

The way in which the tool decides how to classify each station is by looking at the last day's
readings and keeping only those which fall within a limit.  For certain classifications (all except
'Severe'), it is also acceptable if the water level is slightly below the threshold, provided it is
increasing over the past three days, as this could be more vital than a water level within the
threshold, which is currently decreasing, as that would no longer be a threat.

This development is very adaptable, and all of the details of the filtering process have been
outlined at the start of the function.  The system can therefore be adapted easily, for example in
places where resources are more scarce, the thresholds may need to be harsher, so that the resources
can be rationed more carefully.  This program caters for adaptability in places like these, as well
as the other end of the spectrum, where a 'better safe than sorry' outlook may be taken.

The system can be run from Task 2G using packages that come default to Spyder (Python 3.6)