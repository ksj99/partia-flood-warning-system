from floodsystem.stationdata import *
from floodsystem.flood import *
from floodsystem.station import *
from floodsystem.geo import *

def run():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    for item in stations_highest_rel_level(stations, 10):
        print(item)


if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()
