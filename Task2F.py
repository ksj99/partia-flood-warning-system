#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 24 17:25:22 2018

@author: juliettewrixon
"""

from floodsystem.stationdata import *
from floodsystem.flood import *
from floodsystem.station import *
from floodsystem.geo import *
import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import polyfit
from floodsystem.plot import plot_water_level_with_fit
import matplotlib.pyplot as plt



def build_list():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)
    #create list of station with the 5 highest water levels
    highest_stations = []
   
    for item in stations_highest_rel_level(stations, 5):
        highest_stations.append(item[0])
    run(highest_stations, stations)
#create a funcation that pull data for each highest station
def run(highest_stations, stations):

   for i in highest_stations:
       station_name = i
       
       
       # Find station
       station_details = None
       for station in stations:
           if station.name == station_name:
               station_details = station
    
       dt = 2
       dates, levels = fetch_measure_levels(station_details.measure_id,dt=datetime.timedelta(days=dt))
       plt.plot(dates,levels)
       plot_water_level_with_fit(station_details, dates, levels, 4)    
        
if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    highest_stations = build_list()