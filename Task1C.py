from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance, stations_within_radius


def run():
    """Requirements for Task 1C"""

    p = (52.2053, 0.1218)

    # Build list of stations
    stations = build_station_list()

    list = stations_by_distance(stations, p)
    print ("Closest 10 are:\n{}".format(list[:10]))
    print ("Furthest 10 are:\n{}".format(list[-10:]))
    
    print("\n\n")
    
    closeby = stations_within_radius(stations, p, 10)
    print("Stations within 10km radius", closeby)
    


if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()
