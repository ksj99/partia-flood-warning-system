#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 21:13:40 2018

@author: juliettewrixon
"""
from floodsystem.geo import *


def test_station_by_distance():
    A = MonitoringStation (None, None, "A", (50.0, 50.0), None, None, "T1")
    B = MonitoringStation (None, None, "B", (1.0, 1.0), None, None, "T1")
    C = MonitoringStation (None, None, "C", (2.0, 2.0), None, None, "T2")
    test_stations = [A,B,C]
    
    result = stations_by_distance(test_stations, (0,0))
    
    assert [x[0] for x in result] == [B, C, A]
    
def test_stations_within_radius():
    A = MonitoringStation (None, None, "A", (50.0, 50.0), None, None, "T1")
    B = MonitoringStation (None, None, "B", (1.0, 1.0), None, None, "T1")
    C = MonitoringStation (None, None, "C", (2.0, 2.0), None, None, "T2")
    test_stations = [A,B,C]
    
    assert stations_within_radius(test_stations, (50,0), 0.0) == []
    assert stations_within_radius(test_stations, (50,50), 10.0) == [A]
    assert stations_within_radius(test_stations, (50,0), 100.0) == [A, B, C]
    
def test_rivers_with_station():
    A = MonitoringStation (None, None, "A", None, None, "R1", None)
    B = MonitoringStation (None, None, "A", None, None, "R2", None)
    C = MonitoringStation (None, None, "A", None, None, "R3", None)
    D = MonitoringStation (None, None, "A", None, None, "R4", None)
    test_stations = [A,B,C,D]
    
    assert set(rivers_with_station(test_stations)) == set(["R4", "R1", "R3", "R4"]) #check file type
    
def test_stations_by_river():
    A = MonitoringStation ("A", None, "A", None, None, "R1", None)
    B = MonitoringStation ("B", None, "B", None, None, "R2", None)
    C = MonitoringStation ("C", None, "C", None, None, "R3", None)
    D = MonitoringStation ("D", None, "D", None, None, None, None)
    test_stations = [A,B,C,D]
    assert stations_by_river(test_stations) == {'R1':'A', 'R2':'B','R3':'C'}
    
def test_rivers_by_station_number():
    A = MonitoringStation ("A", None, "A", None, None, "R1", None)
    B = MonitoringStation ("B", None, "B", None, None, "R1", None)
    C = MonitoringStation ("C", None, "C", None, None, "R1", None)
    D = MonitoringStation ("D", None, "D", None, None, "R2", None)
    E = MonitoringStation ("E", None, "E", None, None, "R2", None)
    test_stations = [A,B,C,D,E]
    assert rivers_by_station_number(test_stations, 2) == {'R1':3, 'R2':2}    
    
