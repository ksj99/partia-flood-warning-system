#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 09:50:42 2018

@author: juliettewrixon
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib


def polyfit(dates, levels, p):
    x = matplotlib.dates.date2num(dates)
    y = levels
    p_coeff = np.polyfit(x - x[0], y, p)
    poly = np.poly1d(p_coeff)
    
    return poly, x[0]
    
    



