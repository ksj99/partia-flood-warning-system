"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key
from math import radians, cos, sin, asin, sqrt

AVG_EARTH_RADIUS = 6371  # in km
MILES_PER_KILOMETER = 0.621371


def stations_by_distance(stations, p):
    empty = []
    for station in stations:
        name = station.name
        distance =haversine(p,station.coord) #calculate the distance between p and station
        empty.append((name,distance)) #add name of station and it's distance to the empty list
        
    return sorted_by_key(empty,1) #sort empty into ascending order
    

def stations_within_radius(stations, centre,r):
    empty = []
    for station in stations:
        name = station.name
        distance = haversine(centre,station.coord)
        if distance <= r :
            empty.append((name,distance)) #if the distance from the centre to the station is less than the radius then add station to list
    return sorted_by_key(empty,0) #sort list into alphabetical order 


def rivers_with_station(stations): #created for 1D
    rivers = [] #create empty set
    for station in stations:
        rivers.append(station.river) #should not add a river if it is already in the set
    rivers = sorted(list(set(rivers)))
    return rivers
            
def stations_by_river(stations):
    rivers = rivers_with_station(stations)
    
    dict = {}
    for river in rivers:
        names = []
        for station in stations: #go through the stations,
            if river == station.river:  #if the station is on that river, 
                names.append(station.name) #add name to list
            else:
                pass
        names.sort()
        dict[river] = names
        
    return dict

def rivers_by_station_number(stations, N):
    dict = stations_by_river(stations)
    rivers = rivers_with_station(stations)
    river_number = []
    for river in rivers:
        number_of_stations = len(dict[river])
        river_number.append((river, number_of_stations))
    river_number = sorted_by_key(river_number, 1, True)
    #river_number = river_number[::-1] #reverse order
    river_with_fewest_stations = river_number[N-1]
    main_list = river_number[:N] #list of N rivers with most stations
    for item in river_number:
         if item[1] == river_with_fewest_stations[1]:
            main_list.append(item)
    return main_list


def haversine(point1, point2, miles=False): #work out distance
    #calculate distance between point1 and point2
    # unpack latitude/longitude
    lat1, lng1 = point1
    lat2, lng2 = point2

    # convert all latitudes/longitudes from decimal degrees to radians
    lat1, lng1, lat2, lng2 = map(radians, (lat1, lng1, lat2, lng2))

    # calculate haversine
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = sin(lat * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lng * 0.5) ** 2
    h = 2 * AVG_EARTH_RADIUS * asin(sqrt(d))
    if miles:
        return h * MILES_PER_KILOMETER # in miles
    else:
        return h  # in kilometers
