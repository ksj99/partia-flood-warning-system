# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 00:05:30 2018

@author: kiran
"""

from .station import MonitoringStation
from .utils import sorted_by_key


def stations_level_over_threshold(stations, tol, reverse=False):
    list = []
    for station in stations:
        relativeLevel = MonitoringStation.relative_water_level(station)
        if relativeLevel != None:
            if relativeLevel >= tol:
                tuple = (station.name, relativeLevel)
                list.append(tuple)
    list = sorted_by_key(list, 1, reverse)
    return list
        
def stations_highest_rel_level(stations, N):
    list = stations_level_over_threshold(stations, 0)
    shortlist = list[-N:]
    shortlist = sorted_by_key(shortlist, 1, True)
    return shortlist