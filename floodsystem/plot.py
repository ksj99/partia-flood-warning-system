#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 14:55:42 2018

@author: juliettewrixon
"""


from floodsystem.analysis import polyfit
import matplotlib.pyplot as plt
import matplotlib
import numpy as np



def plot_water_level_with_fit(station, dates, levels, p):
    # 1. Plot current data
    
    
    # 2. plot max min line
    
    plt.plot(dates, station.typical_range[1]*np.ones(len(dates)))
    
    plt.plot(dates, station.typical_range[0]*np.ones(len(dates)))
    
    
    
    
    # 3. Plot polynomial
    x = matplotlib.dates.date2num(dates)
    
    x1 = np.linspace(x[0], x[-1], 30)
    poly = polyfit(dates, levels, p)[0]
    plt.plot(x1, poly(x1 - x[0]))
    
    plt.xlabel('date')
    plt.ylabel('water level(m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    plt.tight_layout()
    plt.show()
    