"""Unit test for the station module"""

import pytest
from floodsystem.station import *


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town
    return s

def test_inconsistent_typical_range_stations():
    A = MonitoringStation ("A", None, "A", None, [1,3], None, None)
    B = MonitoringStation ("B", None, "B", None, [1,1], None, None)
    C = MonitoringStation ("C", None, "C", None, [3,1], None, None)
    test_stations = [A,B,C]
    print(inconsistent_typical_range_stations(test_stations))
    assert inconsistent_typical_range_stations(test_stations) == [C] 
    
