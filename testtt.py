# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

print("Testing Python Install")


from floodsystem.flood import *
from floodsystem.stationdata import MonitoringStation

#Create 5 MonitoringStation with known typical ranges
station0 = MonitoringStation('station_id', 'measure_id', 'station0', 'coord', (1,2), 'riverA', 'town')
station1 = MonitoringStation('station_id', 'measure_id', 'station1', 'coord', (1,2), 'riverB', 'town')
station2 = MonitoringStation('station_id', 'measure_id', 'station2', 'coord', (1,2), 'riverC', 'town')
station3 = MonitoringStation('station_id', 'measure_id', 'station3', 'coord', (1,2), 'riverD', 'town')
station4 = MonitoringStation('station_id', 'measure_id', 'station4', 'coord', (1,2), 'riverE', 'town')

#Attach known level data to stations
station0.latest_level = 1
station1.latest_level = 1.5
station2.latest_level = 2
station3.latest_level = 3
station4.latest_level = None
   
#Create a list of the MonitoringStation objects
stations = [station0, station1, station2, station3, station4]

#Use the function to create a list
test_list = stations_level_over_threshold(stations,0.4)
print(test_list)