from floodsystem.stationdata import *
from floodsystem.flood import *
from floodsystem.station import *
from floodsystem.geo import *

def run():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    for item in stations_level_over_threshold(stations, 0.8):
        print(item)


if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()
