from floodsystem.stationdata import *
from floodsystem.geo import *
from floodsystem.station import *


def run():
    """Requirements for Task 1F"""

    p = (52.2053, 0.1218)

    # Build list of stations
    stations = build_station_list()

    # Print number of stations
    print("Number of stations: {}".format(len(stations)))

    # Display data from 3 stations:
    for station in stations:
        if station.name in ['Bourton Dickler', 'Surfleet Sluice', 'Gaw Bridge']:
            print(station)

    list = stations_by_distance(stations, p)
    print ("Closest 10 are:\n{}".format(list[:10]))
    print ("Furthest 10 are:\n{}".format(list[-10:]))

    print("\n\n")

    closeby = stations_within_radius(stations, p, 10)
    print(closeby)

    print("\n\n")

    rws = rivers_with_station(stations)
    print(type(rws))
    print("Number of rivers with at least one station: {}".format(len(rws)))
    #print first ten rivers alphabetically
    print(rws)
    print("first 10 rivers with at least one monitoring station: ", rws[:10])


    print("\n")

    sbr = stations_by_river(stations)

    print("Stations on River Aire: ", sbr["River Aire"])
    print("\nStations on River Cam: ", sbr["River Cam"])
    print("\nStations on River Thames: ", sbr["Thames"])


    print("\n\n")

    task1E = rivers_by_station_number(stations, 9)
    print(task1E)


    print("\n\n")

    inconsistent = inconsistent_typical_range_stations(stations)

    inconsistent_stations = []
    for station in inconsistent:
        inconsistent_stations.append(station.name)

    print(sorted(inconsistent_stations))


if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()
