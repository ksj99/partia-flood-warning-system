#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 24 17:25:22 2018

@author: kiran
"""

from floodsystem.stationdata import *
from floodsystem.flood import *
from floodsystem.station import *
from floodsystem.geo import *
import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import polyfit
from floodsystem.plot import plot_water_level_with_fit
import matplotlib.pyplot as plt



def build_list(doiprint):
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)
    #create list of station with relative water levels above 1
   
    dt = 1
    
    print("\n\n\n")


    """Categories are decided by finding the first applicable label, working down the list:
    
        - SEVERE:    relative level >= 1
        - HIGH:      0.9 <= relative level < 1
                  or 0.85 <= relative level < 0.9 and relative level increasing over 3 days
        - MODERATE:  0.7 <= relative level < 0.9
                  or 0.65 <= relative level < 0.7 and relative level increasing over 3 days
        - LOW:       0.55 <= relative level < 0.7
                  """
    
    
    """ --- SEVERE --- """
       
    severe = stations_level_over_threshold(stations, 1)
    severeList = []
    for item in severe:
        for station in stations:
            if station.name == item[0]:
                dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
                meanlevel = sum(levels)/len(levels)
                meanrellevel = relativeWaterLevel(station, meanlevel)
                severeList.append((item[0], meanrellevel))
    severeList = sorted_by_key(severeList, 1, True)
    if len(severeList) != 0:
        while severeList[len(severeList)-1][1] < 1:
            severeList = severeList[:-1]
        if len(severeList) != 0:
            if doiprint[0] == 1:
                print("Severe risk includes:")
                for i in range(len(severeList)-1):
                    print("   -", severeList[i][0], "at mean relative level", severeList[i][1])
            else:
                pass
    if len(severeList) != 0:
        if doiprint[0] == 1:
            print("\n\n\n")
    

    """ --- HIGH --- """

    high = stations_level_over_threshold(stations, 0.9)[:-(len(severeList))]
    highList = []
    for item in high:
        for station in stations:
            if station.name == item[0]:
                dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
                meanlevel = sum(levels)/len(levels)
                meanrellevel = relativeWaterLevel(station, meanlevel)
                highList.append((item[0], meanrellevel))
    alternate_high = stations_level_over_threshold(stations, 0.8)[:-(len(severeList)+len(highList))]
    alternate_highList = []
    for item in alternate_high:
        for station in stations:
            if station.name == item[0]:
                dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=3))
                if levels[len(levels)-1] > levels[0]:
                    meanlevel = sum(levels)/len(levels)
                    meanrellevel = relativeWaterLevel(station, meanlevel)
                    alternate_highList.append((item[0], meanrellevel))
    highList = sorted_by_key(highList, 1, True)
    alternate_highList = sorted_by_key(alternate_highList, 1, True)
    if len(highList) != 0:
        while highList[len(highList)-1][1] < 0.9:
            highList = highList[:-1]
        if len(highList) != 0:
            if doiprint[1] == 1:
                print("High risk includes:")
                for i in range(len(highList)-1):
                    print("   -", highList[i][0], "at mean relative level", highList[i][1])
    if len(alternate_highList) != 0:
        while alternate_highList[len(alternate_highList)-1][1] < 0.8:
            alternate_highList = alternate_highList[:-1]
        if len(alternate_highList) != 0:
            if doiprint[1] == 1:
                print("As well as:")
                for i in range(len(alternate_highList)-1):
                    print("   -", alternate_highList[i][0], "at mean relative level", alternate_highList[i][1])
    if len(highList) != 0 or len(alternate_highList) != 0:
        if doiprint[1] == 1:
            print("\n\n\n")
    
        
    
    """ --- MODERATE --- """

    moderate = stations_level_over_threshold(stations, 0.7)[:-(len(severeList)+len(highList))]
    for element in moderate:
        for thing in alternate_highList:
            if element == thing:
                moderate.remove(element)
    moderateList = []
    for item in moderate:
        for station in stations:
            if station.name == item[0]:
                dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
                meanlevel = sum(levels)/len(levels)
                meanrellevel = relativeWaterLevel(station, meanlevel)
                moderateList.append((item[0], meanrellevel))
    alternate_moderate = stations_level_over_threshold(stations, 0.65)[:-(len(severeList)+len(highList)+len(alternate_highList)+len(moderateList))]
    alternate_moderateList = []
    for item in alternate_moderate:
        for station in stations:
            if station.name == item[0]:
                dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=3))
                if levels[len(levels)-1] > levels[0]:
                    meanlevel = sum(levels)/len(levels)
                    meanrellevel = relativeWaterLevel(station, meanlevel)
                    alternate_moderateList.append((item[0], meanrellevel))
    moderateList = sorted_by_key(moderateList, 1, True)
    alternate_moderateList = sorted_by_key(alternate_moderateList, 1, True)
    if len(moderateList) != 0:
        while moderateList[len(moderateList)-1][1] < 0.7:
            moderateList = moderateList[:-1]
        if len(moderateList) != 0:
            if doiprint[2] == 1:
                print("Moderate risk includes:")
                for i in range(len(moderateList)-1):
                    print("   -", moderateList[i][0], "at mean relative level", moderateList[i][1])
    if len(alternate_moderateList) != 0:
        while alternate_moderateList[len(alternate_moderateList)-1][1] < 0.65:
            alternate_moderateList = alternate_moderateList[:-1]
        if len(alternate_moderateList) != 0:
            if doiprint[2] == 1:
                print("As well as:")
                for i in range(len(alternate_moderateList)-1):
                    print("   -", alternate_moderateList[i][0], "at mean relative level", alternate_moderateList[i][1])
    if len(moderateList) != 0 or len(alternate_moderateList) != 0:
        if doiprint[2] == 1:
            print("\n\n\n")
            
    """ --- LOW --- """

    low = stations_level_over_threshold(stations, 0.55)[:-(len(severeList)+len(highList)+len(alternate_highList)+len(moderateList))]
    for element in low:
        for thing in alternate_moderateList:
            if element == thing:
                low.remove(element)
    lowList = []
    for item in low:
        for station in stations:
            if station.name == item[0]:
                dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
                meanlevel = sum(levels)/len(levels)
                meanrellevel = relativeWaterLevel(station, meanlevel)
                lowList.append((item[0], meanrellevel))
    lowList = sorted_by_key(lowList, 1, True)
    if len(lowList) != 0:
        while lowList[len(lowList)-1][1] < 0.55:
            lowList = lowList[:-1]
        if len(lowList) != 0:
            if doiprint[3] == 1:
                print("Low risk includes:")
                for i in range(len(lowList)-1):
                    print("   -", lowList[i][0], "at mean relative level",lowList[i][1])
    if len(lowList) != 0 or len(alternate_lowList) != 0:
        if doiprint[3] == 1:
            print("\n\n\n")
            
    print("Done")
            
    
        
def relativeWaterLevel(station, value):
        if MonitoringStation.typical_range_consistent(station) == True:
            if station.latest_level != None:
                a = station.typical_range[0]
                c = station.typical_range[1]
                b = value
                ratio = (b-a)/(c-a)
                return ratio
        else:
            return None
        

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    
    print("See pop up\n")
    
    from tkinter import *
    master = Tk()

    def var_states():
        print("Severe: %d,\nHigh: %d,\nModerate: %d,\nLow: %d" % (var1.get(), var2.get(), var3.get(), var4.get()))

    Label(master, text="Which categories would you like to see?\n Select boxes and close window (X):").grid(row=0, sticky=W)
    var1 = IntVar()
    Checkbutton(master, text="Severe", variable=var1).grid(row=1, sticky=W)
    var2 = IntVar()
    Checkbutton(master, text="High", variable=var2).grid(row=2, sticky=W)
    var3 = IntVar()
    Checkbutton(master, text="Moderate", variable=var3).grid(row=3, sticky=W)
    var4 = IntVar()
    Checkbutton(master, text="Low", variable=var4).grid(row=4, sticky=W)
    Button(master, text='Search', command=var_states).grid(row=5, sticky=W, pady=4)
    mainloop()
    
    highest_stations = build_list([var1.get(), var2.get(), var3.get(), var4.get()])
