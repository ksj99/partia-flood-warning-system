from floodsystem.stationdata import build_station_list
from floodsystem.geo import *


def run():
    """Requirements for Task 1D"""

    p = (52.2053, 0.1218)

    # Build list of stations
    stations = build_station_list()

    # Print number of stations
    print("Number of stations: {}".format(len(stations)))

    # Display data from 3 stations:
    for station in stations:
        if station.name in ['Bourton Dickler', 'Surfleet Sluice', 'Gaw Bridge']:
            print(station)
    
    list = stations_by_distance(stations, p)
    print ("Closest 10 are:\n{}".format(list[:10]))
    print ("Furthest 10 are:\n{}".format(list[-10:]))
    
    print("\n\n")
    
    closeby = stations_within_radius(stations, p, 10)
    print(closeby)
    
    print("\n\n")
    
    rws = rivers_with_station(stations)
    print(type(rws))
    print("Number of rivers with at least one station: {}".format(len(rws)))
    #print first ten rivers alphabetically
    print(rws)
    print("first 10 rivers with at least one monitoring station: ", rws[:10])
    
    
    print("\n")
    
    sbr = stations_by_river(stations)

    print("Stations on River Aire: ", sbr["River Aire"])
    print("\nStations on River Cam: ", sbr["River Cam"])
    print("\nStations on River Thames: ", sbr["Thames"])
    print("\n")


if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()
